package com.itomy.pluck.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

/**
 * Created by naxall on 3/29/16.
 */
@Entity
@Table(name = "user_ref")
public class UserReference {

    @JsonIgnore
    @ManyToOne
    @JoinTable(
        name = "jhi_user",
        joinColumns = {@JoinColumn(name = "user_ref_id", referencedColumnName = "id")})
    private User contact;

    @JsonIgnore
    @ManyToMany
    @JoinTable(
        name = "user_ref_type",
        joinColumns = {@JoinColumn(name = "user_ref_type_id", referencedColumnName = "id")})
    private List<UserReferenceType> userReferenceTypes;

    public User getContact() {
        return contact;
    }

    public void setContact(User contact) {
        this.contact = contact;
    }

    public List<UserReferenceType> getUserReferenceTypes() {
        return userReferenceTypes;
    }

    public void setUserReferenceTypes(List<UserReferenceType> userReferenceTypes) {
        this.userReferenceTypes = userReferenceTypes;
    }
}
