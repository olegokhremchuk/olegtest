package com.itomy.pluck.domain;

import javax.persistence.*;

/**
 * Created by naxall on 3/29/16.
 */
@Entity
@Table(name = "user_ref_type")
public class UserReferenceType {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "type")
    private int type;

    public UserReferenceType() {
    }

    public UserReferenceType(int type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "UserReferenceType{" +
            "id=" + id +
            ", type=" + type +
            '}';
    }
}
